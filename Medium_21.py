operand = ['+', '-', '/', '*']

def calculate(s):
    nums = []
    operands = []
    prev = ''
    isnegative = False
    # loop to seperate the numbers and operands and store them in a list
    for element in s:
        if element is ' ':
            pass
        elif element.isdigit():
            if prev.isdigit():
                x = (abs(int(nums.pop()))) * 10
                x = x + int(element)
                # print(isnegative)
                if isnegative:
                    temp = -x
                    nums.append(str(temp))
                else:
                    nums.append(str(x))
            else:
                if isnegative:
                    temp = -int(element)
                    nums.append(temp)
                else:
                    nums.append(element)
            prev = element
        elif element in operand:
            operands.append(element)
            if element is '+':
                isnegative = False
            elif element is '-':
                isnegative = True
            else:
                isnegative = False
            prev = element
    nums = list(map(int, nums))
    # print(nums, operands)
    count = 0
    indexes = []
    i = 0
    # Loop for taking out indexes of priority operators
    for oper in operands:
        if oper is '*' or oper is '/':
            indexes.append(i)
        i += 1
    i = 0
    # Loop for priority operators to be performed first i.e * and /
    for index in indexes:
        temp1 = nums[index-i]
        temp2 = nums[index-i+1]
        if operands[index] == '*':
            result = temp1 * temp2
        elif operands[index] == '/':
            result = temp1 / temp2
            result = int(result)

        nums.remove(temp1)
        nums.remove(temp2)
        nums.insert(index-i, result)


        i += 1
    # print(nums, operands)
    # returns the sum of numbers in the list as negative sign of numbers was previously catered
    return sum(nums)

    '''
    try:
        while not first and not second:
            # print(save)
            try:
                save = operands.index('/')
                operands.remove('/')
                elem1 = (nums[save])
                elem2 = (nums[save + 1])
                result = elem1 / elem2
                result = int(result)
                nums.remove(elem1)
                # print(nums)
                nums.remove((elem2))
                nums.insert(save, (result))
                print(nums, operands)
            except ValueError:
                first = True
            try:
                save = operands.index('*')
                # print(save)
                operands.remove('*')
                elem1 = (nums[save])
                elem2 = (nums[save + 1])
                result = elem1 * elem2
                # print(elem1, elem2, result)
                nums.remove((elem1))
                nums.remove((elem2))
                # print(nums)
                nums.insert(save, (result))
                print(nums, operands)
            except ValueError:
                second = True

    except ValueError:
        # print("ValueError")
        pass
    
    try:
        while True:
            save = operands.index('*')
            # print(save)
            operands.remove('*')
            elem1 = (nums[save])
            elem2 = (nums[save+1])
            result = elem1 * elem2
            # print(elem1, elem2, result)
            nums.remove((elem1))
            nums.remove((elem2))
            # print(nums)
            nums.insert(save, (result))
            print(nums, operands)
    except ValueError:
        # print("ValueError")
        pass

    
    # print(nums, operands)
    # nums = list(map(float, nums))
    # print(sum(nums), operands)
    return int(sum(nums))
    '''
s = "1+2*5/3+6/4*2"
print(calculate(s))

'''
nums = []
prev = ''
doTask = ''
takenum = True
isnegative = False
for element in s:
    if element.isdigit():
        print(prev.isdigit(), prev)
        if prev.isdigit():
            print(nums)
            x = (abs(float(nums.pop()))) * 10
            x = x + float(element)
            if isnegative:
                temp = -x
                nums.append(str(temp))
            else:
                nums.append(str(x))

            if doTask is not '':
                x = float(nums.pop())
                y = float(nums.pop())
                if doTask is '*':
                    result = x*y
                elif doTask is '/':
                    result = y/x
                nums.append(str(result))
                doTask = ''

        else:
            nums.append(element)
        prev = element
    if element in operand:
        takenum = True
        if element is '+':
            isnegative = False
            doTask = ''
        elif element is '-':
            isnegative = True
            doTask = ''
        elif element is '*':
            doTask = element
            isnegative = False
        elif element is '/':
            doTask = element
            isnegative = False
        prev = element
print(nums)
'''