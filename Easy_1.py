def sortArrayByParity(A):
    Sorted = []
    for element in A:
        #inserts even at the start of list
        if element % 2 == 0:
            Sorted.insert(0, element)
        else:
            Sorted.append(element)
    return Sorted


A = [3,4,2,1]
print(sortArrayByParity(A))