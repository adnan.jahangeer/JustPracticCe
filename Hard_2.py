def checkPalindrome(s_temp):
    length = len(s_temp) - 1
    isPalindrome = True
    z = 0
    # loop for symmetric check
    while z < length / 2:
        if s_temp[z] != s_temp[length-z]:
            isPalindrome = False
            return isPalindrome
        z += 1
    return isPalindrome


def case1(n):
    i = 1

    while i < 100:
        if checkPalindrome(str(int(n) - i)):
            return str(int(n) - i)
        elif checkPalindrome(str(int(n) + i)):
            return str(int(n) + i)
        i += 1

    save = len(n) // 2
    toAppend = n[0:save]
    left = toAppend
    right = toAppend[::-1]
    new = left + n[save] + right
    new2 = left + str(int(n[save])-1) + right
    if new == n:
        char = (str(int(n[save])-1))
        if char[0].isdigit():
            new = left + str(int(n[save])-1) + right
        else:
            new = left + str(int(n[save])+1) + right
    elif abs(int(new) - int(n)) > abs(int(new2) - int(n)):
        print("Hello2")
        return new2
    else:
        pass
    return new


def case2(n):
    i = 1
    while i < 100:
        if checkPalindrome(str(int(n) - i)):
            return str(int(n) - i)
        elif checkPalindrome(str(int(n) + i)):
            return str(int(n) + i)
        i += 1
    try:
        save = len(n) // 2
        toAppend = n[0:save]
        left = toAppend
        right = left[::-1]
        new = left + right
        right2 = right
        ch = str(int(right2[0])-1)
        newstr = ch + right[1:len(right)]

        ch2 = str(int(right2[0]) + 1)
        newstr2 = ch2 + right[1:len(right)]

        new2 = str(int(left)-1) + newstr
        new3 = str(int(left)+1) + newstr2
        try:
            if abs(int(new) - int(n)) >= abs(int(new2) - int(n)):
                return new2
        except ValueError:
            pass

        try:
            if abs(int(new) - int(n)) > abs(int(new3) - int(n)):
                return new3
        except ValueError:
            pass

        return new
    except ValueError:
        return new



def nearestPalindromic(n):
    leng = len(n)
    isOdd = False
    if leng % 2 is not 0:
        isOdd = True
    if isOdd:
        return case1(n)
    else:
        return case2(n)

inp = "835868090839964076"
print("835868091190868538")
print(nearestPalindromic(inp))
