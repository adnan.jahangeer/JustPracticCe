def numUniqueEmails(emails):
    arr = set()
    for email in emails:
        # loop to filter out domain and hostname
        str1 = email.split('@')

        # removes all the dots in the name
        try:
            x = str1[0].split('.')
            str1[0] = ""
            for value in x:
                str1[0] = str1[0]+value
        except ValueError:
            pass

        # Removes everything that is after +
        try:
            y = str1[0].split('+')
            str1[0] = y[0]
        except ValueError:
            pass

        # Adds up the both parts of email
        # Make a set and store the unique name, If already exists set wont add it
        arr.add(str1[0] + '@' + str1[1])

    # Returns the length of set i.e No of unique email address
    return len(arr)
emails = ["test.email+alex@leetcode.com","test.e.mail+bob.cathy@leetcode.com","testemail+david@lee.tcode.com"]
print(numUniqueEmails(emails))