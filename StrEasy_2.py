def isLongPressedName(name, typed):
    i = 0
    j = 0

    # loop for checking of name
    while i < len(name) and j < len(typed):
        if name[i] == typed[j]:
            # if matches proceed to the multiple counts of alphabet
            try:
                # Condition to check if the next alphabet is the same as previous, If it is then skip the counting
                # Exception for the last iteration of loop
                if name[i+1] != name[i]:
                    j += 1
                    tocheck = name[i]
                    # While same alphabets ignore the alphabets of second word
                    while j < len(typed) and tocheck == typed[j]:
                        j += 1
                else:
                    j += 1
            except IndexError:
                j += 1
                tocheck = name[i]
                while j < len(typed) and tocheck == typed[j]:
                    j += 1
        # If not matches return False
        else:
            return False
        i += 1

    # If loop ends on both of the strings finished and no False condition occurs, return True else False
    if i == len(name) and j == len(typed):
        return True
    else:
        return False



name = "Alex"
typed = "AAAlleeexxx"
print(isLongPressedName(name, typed))