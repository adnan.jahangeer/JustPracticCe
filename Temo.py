def checkvalid(tocheck):
    leng = len(tocheck)
    openb = 0
    for i in range(0, leng):
        if tocheck[i] == '(':
            openb += 1
        elif tocheck[i] == ')':
            openb -= 1

        if openb < 0:
            return False
    if openb == 0:
        return True
    else:
        return False



from itertools import combinations


set1 = ['(', '(', '(', '(', '(', '(', ')', ')', ')', ')', ')', ')']
arr = set()
i = 0
for subset in combinations(set1, 6):
    temp = subset[0] + subset[1] + subset[2] + subset[3] + subset[4] + subset[5]
    if checkvalid(temp):
        arr.add(temp)
    i += 1
print(arr)
print(i)
