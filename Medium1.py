import collections

def leastInterval(tasks, n):
    save = dict()
    # Calculation of frequency of alphabets
    for element in tasks:
        if element in save:
            save[element] += 1
        else:
            save[element] = 1
    minimum = len(tasks)
    longest = ""
    longestc = 0
    # finding largest frequency to calculate maximum idle spots
    for element in save:
        if longestc < save[element]:
            longestc = save[element]
            longest = element

    # Largest possible with maximum idle
    idle = n * (longestc-1)
    print(idle)
    # Filling the idle slots by other tasks
    for key, value in save.items():
        if key != longest and idle >= 0:
            if longestc-1 < value:
                idle -= longestc - 1
            else:
                idle -= value
    if idle < 0:
        idle = 0

    total = minimum + idle
    return total


tasks = ["A","A","A","B","B","B"]
n = 0
print(leastInterval(tasks, n))


'''
    index = 0
    total = 0
    while index < len(tasks):
        save = []
        save.append(tasks[index])
        toAdd = 3

        try:
            i = index
            cond = False
            toAdd = 3
            while tasks[i] in save and i < len(tasks):
                i += 1
            if i < len(tasks):
                save.append(tasks[i])
                cond = True
                tasks.pop(i)
                toAdd -= 1
            j = i
            cond = False

            while tasks[j] in save and j < len(tasks):
                j += 1
            if j < len(tasks):
                cond = True
                tasks.pop(j)
                toAdd -= 1
        except IndexError:
            pass
        index += 1
        total += toAdd
        print(total, tasks)
    return total
    '''