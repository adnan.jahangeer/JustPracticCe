def removeElement(nums, val):
    try:
        # Removes value till exception occurs
        while True:
            nums.remove(val)
    except ValueError:
        # When all values removed, return the new length
        return len(nums)



A = [3,3]
val = 3
print(removeElement(A, val))