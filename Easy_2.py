import math

def maxDistToClosest(seats):
    maxZero = 0
    index = 0
    savecountone = 0
    currZero = 0
    countone = 0
    cond = False
    # loop to check the maximum zero count in the list
    while index < len(seats):
        endwithzero = False
        if seats[index] == 0:
            currZero += 1
            if not cond:
                maxZero += 1
            else:
                if currZero > maxZero:
                    savecountone = countone
                    maxZero = currZero
                    endwithzero = True
                elif currZero == maxZero:
                    maxZero = currZero
                    endwithzero = True

        else:
            countone += 1
            currZero = 0
            cond = True
        index += 1

    print(endwithzero, savecountone)

    if endwithzero or savecountone == 0:
        return maxZero
    else:
        return math.ceil(maxZero/2)


A  =  [0,0,0,1,0,0,0,1,0,0,0,0,1,1,0,0,0,1]

print(maxDistToClosest(A))